const { get, set } = require('lodash');
const translations = require('../translations');

const isAvailable = (available = [], values = []) => {
  return values.every(value => available.includes(value))
};


const getAvailableLanguages = () => {
  try {
    return Object.keys(translations);
  } catch (error) {
    throw new Error('BAD_TRANSLATIONS_FILE')
  }
};

const getAvailableNamespaces = () => {
  try {
    return Object.values(translations).reduce((result, languageObj) => {
      for (let namespace in languageObj) {
        if (!result.includes(namespace)) {
          result.push(namespace);
        }
      }

      return result;
    }, []);
  } catch (e) {
    throw new Error('BAD_TRANSLATIONS_FILE')
  }
};

const filterTranslations = (languages = [], namespaces = []) => {
  try {
    return languages.reduce((result, language) => {
      namespaces.forEach((namespace) => {
        const data = get(translations, `${language}.${namespace}`);
        set(result, `${language}.${namespace}`, data)
      });

      return result;
    }, {});
  } catch(error) {
    throw new Error('BAD_TRANSLATIONS_FILE')
  }
};

const generateOutput = (namespaces, languages) => {
  const allNamespaces = getAvailableNamespaces();
  const allLanguages = getAvailableLanguages();

  if (languages) {
    try {
      languages = languages.split(',');
    } catch (error) {
      throw new Error("BAD_LANGUAGES_INPUT")
    }

    if (!isAvailable(allLanguages, languages)) {
      throw new Error("UNKNOWN_LANGUAGE")
    }

  } else {
    languages = allLanguages
  }

  if (namespaces) {
    try {
      namespaces = namespaces.split(',');
    } catch (error) {
      throw new Error("BAD_NAMESPACES_INPUT")
    }

    if (!isAvailable(allNamespaces, namespaces)) {
      throw new Error("UNKNOWN_NAMESPACE")
    }

  } else {
    namespaces = allNamespaces
  }

  return filterTranslations(languages, namespaces);
};


module.exports = {
  generateOutput,
  getAvailableLanguages,
  getAvailableNamespaces
};
