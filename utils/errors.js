const standardErrorResponse = ({ response, title, message, status }) => {
  response.status(status).send({
    errors: [{
      "error": title,
      "message": message,
      "status": status,
      "timestamp": new Date()
    }]
  });
};

module.exports = {
  standardErrorResponse
};
