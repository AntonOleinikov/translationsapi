const { SecretManagerServiceClient } = require(`@google-cloud/secret-manager`);
const client = new SecretManagerServiceClient();
const { standardErrorResponse } = require('../utils/errors');

const authenticate = async (req, res, next) => {
  if (process.env.NODE_ENV !== `production`) {
    return next();
  }

  try {
    const [accessData] = await client.accessSecretVersion({
      "name": "projects/787614996778/secrets/TRANSLATIONS_SERVICE_ACCESS_TOKEN/versions/latest"
    });

    const accessToken = accessData.payload.data.toString();
    const authorization = req.header('authorization').split(' ')[1];

    if (
      accessToken &&
      authorization &&
      authorization === accessToken
    ) {
      return next();
    }

    throw new Error('UNAUTHENTICATED');

  } catch(error) {
    standardErrorResponse({
      "response": res,
      "title": "ERROR",
      "message": error.message,
      "status": 401
    })
  }
};

module.exports = authenticate;
