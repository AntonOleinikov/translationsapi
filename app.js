const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const authenticate = require('./auth');

const languagesRouter = require('./routes/languages');
const namespacesRouter = require('./routes/namespaces');
const sourcesRouter = require('./routes/sources');
const translationsRouter = require('./routes/translations');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(authenticate);

app.use('/', translationsRouter);
app.use('/sources', sourcesRouter);
app.use('/languages', languagesRouter);
app.use('/namespaces', namespacesRouter);

module.exports = app;
