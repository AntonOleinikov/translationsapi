const fs = require('fs');
const axios = require('axios');
const { get, clone } = require('lodash');
const { csv2json } = require('json-2-csv');
const sources = require('./sources');
const OUTPUT_PATH = `${process.cwd()}/translations.json`;


const downloadCSV = async (namespace, downloadURL) => {
  try {
    console.log(`[UPLOAD TRANSLATIONS]: Upload ${namespace} from ${downloadURL}`);

    const { data: csv } = await axios(downloadURL);
    return Promise.resolve({
      csv,
      namespace
    })
  } catch (error) {
    console.log(`[UPLOAD TRANSLATIONS]: ${error}`);
  }
};

const toi18nFormat = (jsonArr, defaultArr = {}, namespace) => {
  return jsonArr.reduce((result, row) => {
    for (let columnName in row) {
      if (
        row.hasOwnProperty(columnName) &&
        isLanguageCode(columnName)
      ) {

        const value = row[columnName];

        result[columnName] = {
          ...get(result, [columnName], {}),
          [namespace]: {
            ...get(result, [columnName, namespace], {}),
            // If there is no utils for this identifier, then we take from translate from "en"
            [row.identifier]: value ? value : row['en']
          }
        }
      };
    }

    return result
  }, clone(defaultArr))
};

const convertCSVtoJSON = (csvData, cb) => {
  let result = {};

  csvData.forEach((csvItem, index) => {
    csv2json(csvItem.csv, (error, csvArr) => {
      if (!error) {
        result = toi18nFormat(csvArr, result, csvItem.namespace);
      }

      if (index === csvData.length - 1) {
        return cb(result);
      }
    })
  });
};

const isLanguageCode = (key) => {
  const exceptions = ['identifier', 'comment', 'lastEdited', 'namespace'];
  return key && key.length < 5 && !exceptions.includes(key);
};

const run = async () => {
  try {

    if (!sources || !sources.length) {
      return console.error(`[UPLOAD TRANSLATIONS]: Sources list doesn't exist or empty.`);
    }

    console.log(`[UPLOAD TRANSLATIONS]: Start uploading...`);

    const csvData = await Promise.all(
      sources.map(({downloadURL, namespace}) => downloadCSV(namespace, downloadURL))
    );

    convertCSVtoJSON(csvData, (data) => {

      fs.writeFile(OUTPUT_PATH, JSON.stringify(data, null, 2), (error) => {
        if (error) {
          return console.log(`[DOWNLOAD TRANSLATIONS]: Failed to save JSON. Error: ${error}`);
        }
        console.log(`[DOWNLOAD TRANSLATIONS]: Uploading finished. Translations  saved to ${OUTPUT_PATH}`);
      });
    });


  } catch (error) {
    console.error(`[DOWNLOAD TRANSLATIONS]: Error: ${error}`);
  }
};

if (process.argv.includes('run')) {
  run();
}
