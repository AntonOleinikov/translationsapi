const csvSources = [
  {
    "namespace": `authentication`,
    "downloadURL": `https://docs.google.com/spreadsheets/d/1VIpkcIm8qIAcx1LPZlE2s1iZkIIEf2Yy8RMfEmFy1D0/gviz/tq?tqx=out:csv`
  },
  {
    "namespace": `dashboard`,
    "downloadURL": `https://docs.google.com/spreadsheets/d/1Lmjnnf7I3fXpQv7Cfs-MYJFwlkeEYkceZCYulnI24J8/gviz/tq?tqx=out:csv`
  },
  {
    "namespace": `tasksManager`,
    "downloadURL": `https://docs.google.com/spreadsheets/d/1413d2QHwKri6e61eFSoDBb0KIPW7iMDrdpftSH10IYw/gviz/tq?tqx=out:csv`
  },
];

module.exports = csvSources;
