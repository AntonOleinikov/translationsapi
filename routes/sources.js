const express = require('express');
const router = express.Router();
const sources = require('../upload/sources');
const { standardErrorResponse } = require('../utils/errors');

router.get('/', (req, res, next) => {
  try {
    res.send(sources);
  } catch (error) {
    standardErrorResponse({
      "response": res,
      "title": "ERROR",
      "message": error.message,
      "status": 500
    })
  }
});

module.exports = router;
