const express = require('express');
const router = express.Router();
const { generateOutput } = require('../utils/translations');
const { standardErrorResponse } = require('../utils/errors');

router.get('/', (req, res, next) => {
  try {
    const { namespaces, languages } = req.query;
    const json = generateOutput(namespaces, languages);

    res.send(json);
  } catch (error) {
    standardErrorResponse({
      "response": res,
      "title": "ERROR",
      "message": error.message,
      "status": 400
    })
  }
});

module.exports = router;
