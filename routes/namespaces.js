const express = require('express');
const router = express.Router();
const { getAvailableNamespaces } = require('../utils/translations');
const { standardErrorResponse } = require('../utils/errors');

router.get('/', (req, res, next) => {
  try {
    const json = getAvailableNamespaces();
    res.send(json);
  } catch (error) {
    standardErrorResponse({
      "response": res,
      "title": "ERROR",
      "message": error.message,
      "status": 500
    })
  }
});

module.exports = router;
